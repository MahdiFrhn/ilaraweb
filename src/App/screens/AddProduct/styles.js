import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  root: {
    display: 'flex',
    direction: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  paper: {
    marginTop: 80,
    padding: 20,
    boxShadow: '0px 0px 2px 0px',
    borderRadius: 15,
    padding: 10,
    textAlign: 'center',
    borderRadius: 10,
  },
  Button: {
    borderRadius: 15,
  },
  imgPaper: {
    width: '100%',
    height: 150,
    borderRadius: 15,
    boxShadow: '0px 0px 4px 0px #000',
  },

  imgcontainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  submitButton: {
    marginTop: 25,
    borderRadius: 15,
  },
}));
