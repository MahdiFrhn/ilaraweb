import { makeStyles } from '@material-ui/core/styles';
const drawerWidth = 190;
export default makeStyles((theme) => ({
  root: {
    direction: 'rtl',
    display: 'flex',
  },
  logo: {
    width: 150,
    height: 40,
    margin: 10,
  },
  logoApp: {
    width: 150,
    height: 40,
  },

  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    backgroundColor: '#cd5d7d',
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% px)`,
    },
  },
  menuButton: {
    marginRight: theme.spacing(1),
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
    
  },

  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));
