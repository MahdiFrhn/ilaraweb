import React, { useState } from 'react';
import { AppBar, Drawer, Hidden, Toolbar, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import styles from './styles';

export default function Menu(props) {
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const classes = styles();
  return (
    <div className={classes.root}>
      <Hidden lgUp>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <MenuIcon
              onClick={handleDrawerToggle}
              className={classes.menuButton}></MenuIcon>
            <Typography variant="h5" color="initial">
              داشبورد مدیریت
            </Typography>
          </Toolbar>
        </AppBar>
      </Hidden>

      <nav className={classes.drawer}>
        <Hidden >
          <Drawer
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true,
            }}>
            {props.side}
          </Drawer>
        </Hidden>
        <Hidden mdDown>
          <Drawer className={classes.drawerPaper} variant="permanent" open>
            {props.side}
          </Drawer>
        </Hidden>
      </nav>
    </div>
  );
}
