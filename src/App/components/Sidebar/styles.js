import { makeStyles } from '@material-ui/core/styles';
export default makeStyles(() => ({
  root: {
    direction: 'rtl',
    marginTop: 30,
  },

  logo: {
    width: 150,
    height: 40,
    margin: 10,
  },
  logoApp: {
    width: 150,
    height: 40,
  },

  item: {
    color: '#137285',
    '&:hover': {
      color: '#fff',
      backgroundColor: '#a83264',
      borderRadius: 5,
    },
  },

  dvider: {
    marginBottom: 60,
  },
  title:{
    display: 'flex',
    justifyContent: 'center',
  }
}));
