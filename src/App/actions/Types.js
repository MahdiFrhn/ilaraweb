export default {
  ADD_PRODUCT: 'ADD_PRODUCT',
  EDIT_PRODUCT: 'EDIT_PRODUCT',
  SET_PRODUCT: 'SET_PRODUCT',
  SET_LOADING: 'SET_LOADING',
  SET_ORDERS: 'SET_ORDERS',
  SET_CATEGORIES: 'SET_CATEGORIES',
};
