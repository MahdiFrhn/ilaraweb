import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  footercls: {
    marginTop: 40,
    marginBottom: 40,
  },
}));
