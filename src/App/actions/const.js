// import WooCommerceAPI from 'react-native-woocommerce-api';

// export const WooAPI = new WooCommerceAPI({
//   url: 'https://mahan-store.ir', // Your store URL
//   ssl: true,
//   consumerKey: 'ck_f84822f31f9a09683684eecde2a0d8532c31ebbb', // Your consumer secret
//   consumerSecret: 'cs_3558dc8f834d9e3b7941500bdc2a0d651ffdff0f', // Your consumer secret
//   wpAPI: true, // Enable the WP REST API integration
//   version: 'wc/v3', // WooCommerce WP REST API version
//   queryStringAuth: true,
// });

import WooCommerceRestApi from "@woocommerce/woocommerce-rest-api";
 
export const WooAPI = new WooCommerceRestApi({
  url: "https://mahan-store.ir",
  consumerKey: 'ck_f84822f31f9a09683684eecde2a0d8532c31ebbb',
  consumerSecret: "cs_3558dc8f834d9e3b7941500bdc2a0d651ffdff0f",
  version: "wc/v3",
  queryStringAuth: true,
});