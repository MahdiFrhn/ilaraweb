import React from 'react';
import { Bar, Pie } from 'react-chartjs-2';
import styles from './styles';
import { Grid } from '@material-ui/core';
const font2 = 'Shabnam';

export default (props) => {
  //for count and show categories
  const category = props.categories.map((i) => i.name);
  const categoryCount = props.categories.map((i) => i.count);
  //for show orders by Date
  const orders = props.orders.map((i) => i.date_created);
  const orderCount = orders.map((i) => i.line_items);
  console.log('orderCount=', orderCount.length);

  const categoriesState = {
    labels: category,
    datasets: [
      {
        backgroundColor: '#449c91',
        borderColor: '#ffff',
        borderWidth: 1,
        data: categoryCount,
      },
    ],
  };

  const ordesrState = {
    labels: ['تعداد سفارشات', 'تعداد محصولات'],
    datasets: [
      {
        label: ['نسبت تعداد سفارشات به تعداد محصولات '],
        backgroundColor: ['#B21F00', '#1b5c5c'],

        borderWidth: 1,
        data: [orderCount.length, props.productsCount],
      },
    ],
  };

  const cls = styles();
  return (
    <div className={cls.root}>
      <Grid
        container
        spacing={1}
        direction="row"
        justify="center"
        alignItems="center">
        <Grid item xs={12} sm={12}>
          <Bar
            data={categoriesState}
            width={"100%"}
            height={350}
            options={{
              maintainAspectRatio: false,
              scales: {
                xAxes: [
                  {
                    gridLines: {
                      display: true,
                    },
                    ticks: {
                      fontColor: '#ffff',
                      fontSize: 10,
                      fontFamily: font2,
                    },
                  },
                ],
                yAxes: [
                  {
                    display: true,
                    gridLines: {
                      display: true,
                    },
                    ticks: {
                      fontColor: '#ffff',
                      fontSize: 10,
                      fontFamily: font2,
                    },
                  },
                ],
              },
              title: {
                display: true,
                text: 'دسته بندی محصولات',
                fontSize: 14,
                fontColor: '#fff',
                fontFamily: font2,
              },
              legend: {
                display: false,
                position: 'right',
                labels: {
                  fontColor: '#fff',
                  fontSize: 10,
                  fontFamily: font2,
                },
              },
            }}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <Pie
            data={ordesrState}
            width={100}
            height={300}
            options={{
              maintainAspectRatio: false,

              title: {
                display: true,
                text: ' نسبت تعداد محصولات به تعداد سفارشات',
                fontSize: 14,
                fontColor: '#fff',
                fontFamily: font2,
              },
              legend: {
                display: true,
                position: 'right',
                width: 100,
                labels: {
                  fontColor: '#fff',
                  fontSize: 10,
                  fontFamily: font2,
                },
              },
            }}
          />
        </Grid>
      </Grid>
    </div>
  );
};
