import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  root: {
    marginTop: 65,
    marginBottom: 35,
  },
  divider:{
   marginTop: 4,
   marginBottom: 4,
  },
  paper: {
    padding: 10,
    textAlign: 'center',
    borderRadius: 10,
  },
  containerpaper: {
    padding: 10,
    height: '100%',
    backgroundColor: '#654062',
    borderRadius: 10,
  },
}));
