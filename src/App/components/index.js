import SideBar from './Sidebar';
import Menu from './Menu';
import Footer from './Footer';
import ProductCard from './ProductCard';
import Edit from './Edit';
import Chart from './Chart';
import OrdersTabel from './OrdersTabel';
import UploadImages from './UploadImages';

export {
  SideBar,
  Menu,
  Footer,
  ProductCard,
  Edit,
  Chart,
  OrdersTabel,
  UploadImages,
};
