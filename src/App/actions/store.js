import { createStore } from 'redux';
import { Reducer } from './reducer';
// import logger from 'redux-logger';
import { devToolsEnhancer } from 'redux-devtools-extension/logOnlyInProduction';

// const devMode = process.env.NODE_ENV === 'development';
// const middleware = devMode ? applyMiddleware(logger) : applyMiddleware();

// export const store = createStore(Reducer, middleware);

export const store = createStore(
  Reducer,
  /* preloadedState, */ devToolsEnhancer()
);
