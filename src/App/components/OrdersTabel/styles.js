import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  container: {
    display: 'flex',
    overflowX: 'hide',
  },
  table: {
    minWidth: 300,
  },
  tableCell: {
    paddingRight: 4,
    paddingLeft: 5,
  },
}));
