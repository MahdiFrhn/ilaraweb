import React, { useEffect, useState } from 'react';
import { loadProducts, loadOrders, loadCategories } from '../../actions';
import { useSelector } from 'react-redux';
import { Chart } from '../../components';
import { Divider, Grid, Container, Paper, Typography } from '@material-ui/core';

import styles from './styles';

export default () => {
  const cls = styles();
  const [totalItems, setTotalItems] = useState();
  const orderList = useSelector((state) => state.orders);
  const categoriesList = useSelector((state) => state.categories);

  //for count Orders
  const orderLength = orderList.length;
  //for count Categories
  const categoriesLength = categoriesList.length;
  //for % count orders
  const percent = (orderLength / totalItems) * 100;

  useEffect(() => {
    // load data from API
    loadProducts()
      .then((data) => {
        console.log('data=', data);
        setTotalItems(data.totalItems);
      })
      .catch((err) => console.log(err));
    loadOrders();
    loadCategories();
  }, []);

  return (
    <div className={cls.root}>
      <Container maxWidth="md">
        <Paper className={cls.containerpaper}>
          <Grid
            container
            spacing={1}
            direction="row"
            justify="flex-start"
            alignItems="flex-start">
            <Grid item xs={6} sm={3}>
              <Paper className={cls.paper}>
                <Typography
                  dir="rtl"
                  variant="body2"
                  color="initial"
                  gutterBottom>
                  فروش به محصول
                </Typography>
                <Divider className={cls.divider} />
                <Typography dir="rtl" variant="h2" color="initial" gutterBottom>
                  {Math.ceil(percent)} %
                </Typography>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={cls.paper}>
                <Typography
                  dir="rtl"
                  variant="body2"
                  color="initial"
                  gutterBottom>
                  دسته بندی ها
                </Typography>
                <Divider className={cls.divider} />
                <Typography dir="rtl" variant="h2" color="initial" gutterBottom>
                  {categoriesLength}
                </Typography>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={cls.paper}>
                <Typography
                  dir="rtl"
                  variant="body2"
                  color="initial"
                  gutterBottom>
                  سفارشات
                </Typography>
                <Divider className={cls.divider} />
                <Typography dir="rtl" variant="h2" color="initial" gutterBottom>
                  {orderLength}
                </Typography>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={cls.paper}>
                <Typography
                  dir="rtl"
                  variant="body2"
                  color="initial"
                  gutterBottom>
                  محصولات
                </Typography>
                <Divider className={cls.divider} />
                <Typography dir="rtl" variant="h2" color="initial" gutterBottom>
                  {totalItems}
                </Typography>
              </Paper>
            </Grid>
          </Grid>
          <Grid item xs={12}>
          <Chart
            orders={orderList}
            productsCount={totalItems}
            categories={categoriesList}
          />
          </Grid>
        </Paper>
      </Container>

    </div>
  );
};
