import SC from './screens';

export default [
  {
    path: '/',
    component: SC.Home,
  },
  {
    path: '/Products',
    component: SC.Products,
  },
  {
    path: '/AddProduct',
    component: SC.AddProduct,
  },
  {
    path: '/Category',
    component: SC.Category,
  },
  {
    path: '/Setting',
    component: SC.Setting,
  },
  {
    path: '/Orders',
    component: SC.Orders,
  },
];
