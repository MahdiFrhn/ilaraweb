import Types from './Types';
import { store } from './store';
import { WooAPI } from './const';

export const editProduct = (payload) =>
  store.dispatch({
    type: Types.EDIT_PRODUCT,
    payload,
  });
export const addProduct = (payload) =>
  store.dispatch({
    type: Types.ADD_PRODUCT,
    payload,
  });

export const setProduct = (payload) =>
  store.dispatch({
    type: Types.SET_PRODUCT,
    payload,
  });

export const setIsLoading = (payload) =>
  store.dispatch({
    type: Types.SET_LOADING,
    payload,
  });

export const setOrderList = (payload) =>
  store.dispatch({
    type: Types.SET_ORDERS,
    payload,
  });
export const setCategories = (payload) =>
  store.dispatch({
    type: Types.SET_CATEGORIES,
    payload,
  });

//----------------------------------------//
//Load Data from API
export const loadProducts = (pageNumber) => {
  return WooAPI.get('products', {
    per_page: 20, // 20 products per page
    page: pageNumber,
  })
    .then((res) => {
      // Successful request
      setProduct(res.data);
      return {
        data: res.data,
        totalPages: res.headers['x-wp-totalpages'],
        totalItems: res.headers['x-wp-total'],
      };
    })
    .catch((error) => {
      console.log('Response Data:', error.res.data);
    });
};

//Orders List load from API
export const loadOrders = () => {
  WooAPI.get('orders')
    .then((res) => {
      setOrderList(res.data);
      console.log('orderList=', res.data);
      return res.data;
    })
    .catch((error) => {
      console.log(error.res.data);
    });
};
//Categories List load from API
export const loadCategories = () => {
  WooAPI.get('products/categories', {
    per_page: 50, // 20 products per page
  })
    .then((res) => {
      setCategories(res.data);
      console.log('categories', res.data);
      return res.data;
    })
    .catch((error) => {
      console.log(error.res.data);
    });
};
//-----------------------------------------//
//add products to API
export const addProducts = (data) => {
  WooAPI.post('products', data)
    .then((res) => {
      console.log('action=', res.data);
      // window.location.reload();
      return res.data;
    })
    .catch((error) => {
      console.log(error.res.data);
    });
};

//edit products to API
export const editProducts = (id, data) => {
  WooAPI.put('products/' + id, data)
    .then((res) => {
      console.log(res.data);
      window.location.reload();
      return res.data;
    })
    .catch((error) => {
      console.log(error.res.data);
    });
};

//Delete product from API List
export const deleteProducts = (id) => {
  WooAPI.delete('products/' + id, {
    force: true,
  })
    .then((res) => {
      console.log(res.data);
      window.location.reload();
      return res.data;
    })
    .catch((error) => {
      console.log(error.res.data);
    });
};
