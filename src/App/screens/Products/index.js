import React, { useEffect, useState } from 'react';
import { Divider, CircularProgress, Grid, Container } from '@material-ui/core';
import { ProductCard } from '../../components';
import styles from './styles';
import { useSelector } from 'react-redux';
import { setIsLoading } from '../../actions';
import { loadProducts } from '../../actions';
import { store } from '../../actions/store';
import defimage from '../../assets/defimage.png';
import Pagination from '@material-ui/lab/Pagination';

export default function () {
  const cls = styles();
  const [pageData, setPageData] = useState(1);
  const [totalPages, setTotalPages] = useState();
  const productList = useSelector((state) => state.products);
  const isLoading = store.getState().loading;

  //image Src Is Null ?
  const imageArray = productList.map((i) => i.images);
  imageArray.forEach((i) => {
    if (i.length === 0) {
      i.push({ src: defimage });
    }
  });
  const pageNumber = (val) => {
    setPageData(val);
  };

  useEffect(() => {
    //load data from API
    loadProducts(pageData)
      .then((data) => {
        console.log('data=', data);
        setTotalPages(data.totalPages);
      })
      .catch((err) => console.log(err));
    setIsLoading(false);
  }, [pageData]);

  return (
    <div>
      <Divider className={cls.divider} />
      <Container maxWidth="md">
        <Grid container justify="center" alignItems="center" spacing={3}>
          {isLoading ? <CircularProgress color="primary" /> : null}
          {productList.map((i, idx) => (
            <Grid item xs={12} sm={6} md={4} key={idx}>
              <ProductCard
                id={i.id}
                title={i.name}
                price_html={i.price_html}
                price={i.price}
                status={i.status}
                image={i.images[0].src}
                quantity={i.stock_quantity}
              />
            </Grid>
          ))}
        </Grid>
        <Divider className={cls.divider} />
        <Grid container justify="center">
          <Pagination
            count={Number(totalPages)}
            variant="outlined"
            color="secondary"
            onChange={(event, val) => pageNumber(val)}
          />
          <Divider className={cls.divider} />
        </Grid>
      </Container>
    </div>
  );
}
