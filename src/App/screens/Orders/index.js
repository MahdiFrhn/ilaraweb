import React from 'react';
import { OrdersTabel } from '../../components';
import { useSelector } from 'react-redux';
import Container from '@material-ui/core/Container';
import styles from "./styles"

export default () => {
    const cls=styles()
  const orderList = useSelector((state) => state.orders);

  return (
    <Container maxWidth="md" className={cls.Container}>
      <OrdersTabel orders={orderList} />
    </Container>
  );
};
