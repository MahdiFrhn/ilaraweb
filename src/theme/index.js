import defaultTheme from './default';
import dark from './dark';
import './index.css';
import { createMuiTheme } from '@material-ui/core';
//fonts const font1 = 'Vazir';
const font1 = 'Vazir';
const font2 = 'Shabnam';

const overrides = {
  typography: {
    fontFamily: font1,
    body2: {
      fontFamily: font2,
      fontSize: '16px',
    },
    body1: {
      fontFamily: font2,
    },
    h1: {
      fontSize: '3rem',
    },
    h2: {
      fontSize: '14px',
      fontFamily: font2,
    },
    h3: {
      fontSize: '1.64rem',
      fontFamily: font2,
    },
    h4: {
      fontSize: '1.5rem',
    },
    h5: {
      fontSize: '1.285rem',
    },
    h6: {
      fontFamily: font2,
      fontSize: '1.142rem',
    },
  },
};

export default {
  default: createMuiTheme({ ...defaultTheme, ...overrides }),
  dark: createMuiTheme({ ...dark, ...overrides }),
};
