import React, { useState } from 'react';
import {
  Grid,
  Paper,
  TextField,
  Container,
  Button,
  Typography,
  RadioGroup,
  Radio,
  FormControlLabel,
} from '@material-ui/core';
import ImageUploading from 'react-images-uploading';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import UpdateIcon from '@material-ui/icons/Update';
import BackupIcon from '@material-ui/icons/Backup';
import defimage from '../../assets/defimage.png';
import { StylesProvider, jssPreset } from '@material-ui/core/styles';
import { create } from 'jss';
import rtl from 'jss-rtl';
import styles from './styles';
import { addProducts } from '../../actions';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

// Configure JSS for RTL Text field label
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

export default () => {
  const cls = styles();
  const setData = (val) => setState({ ...state, ...val });
  const [images, setImages] = useState([]);
  const [statusValue, setStatusValue] = useState('publish');

  const maxNumber = 10;
  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    console.log(imageList, addUpdateIndex);
    setImages(imageList);
  };
  //value for Radio Button
  const statusChange = (e) => {
    setStatusValue(e.target.value);
  };
  const imageURL = images.map((i) => i.data_url);
  const [state, setState] = useState({
    name: ' ',
    regular_price: '',
    status: statusValue,
    stock_quantity: 1,
    description: '',
    images: [
      {
        src:
          '        https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png',
      },
    ],
  });

  console.log(state.stock_quantity);
  const setToDataBase = () => {
    addProducts(state);
  };

  return (
    <Grid
      container
      spacing={3}
      direction="column"
      justify="flex-start"
      alignItems="flex-start">
      <Container maxWidth="md">
        <Paper className={cls.paper}>
          <Grid item>
            <ImageUploading
              multiple
              value={images}
              onChange={onChange}
              maxNumber={maxNumber}
              dataURLKey="data_url">
              {({
                imageList,
                onImageUpload,
                onImageRemoveAll,
                onImageUpdate,
                onImageRemove,
                isDragging,
                dragProps,
              }) => (
                <div className={cls.imgroot}>
                  {imageList.map((image, index) => (
                    <Grid
                      key={index}
                      container
                      spacing={3}
                      direction="row"
                      justify="center"
                      alignItems="center">
                      <img src={image['data_url']} alt="" width="100" />
                      <Grid item>
                        <Button
                          className={cls.Button}
                          color="secondary"
                          size="small"
                          onClick={() => onImageUpdate(index)}>
                          <UpdateIcon />
                        </Button>
                        <Button
                          className={cls.Button}
                          size="small"
                          color="secondary"
                          onClick={() => onImageRemove(index)}>
                          <DeleteForeverIcon />
                        </Button>
                      </Grid>
                    </Grid>
                  ))}
                  <Paper
                    className={cls.imgPaper}
                    {...dragProps}
                    onClick={onImageUpload}
                    style={isDragging ? { backgroundColor: 'red' } : undefined}>
                    <BackupIcon
                      color="secondary"
                      fontSize="large"
                      style={{ marginTop: 20 }}
                    />
                    <Typography
                      variant="body2"
                      color="primary"
                      style={{ marginTop: 20 }}>
                      عکس را با کشیدن اینجا رها کنید یا کلیک کنید
                    </Typography>
                  </Paper>

                  {/* <Button
                  className={cls.Button}
                  color="secondary"
                  variant="outlined"
                  onClick={onImageRemoveAll}>
                  پاک کردن همه ی عکس ها
                </Button> */}
                </div>
              )}
            </ImageUploading>
          </Grid>
          <Grid item>
            <StylesProvider jss={jss}>
              <TextField
                dir="rtl"
                id="name"
                margin="dense"
                label="نام محصول"
                color="secondary"
                variant="outlined"
                onChange={(e) => setData({ name: e.target.value })}
                fullWidth
              />
              <TextField
                id="price"
                label="قیمت"
                margin="dense"
                color="secondary"
                variant="outlined"
                onChange={(e) => setData({ regular_price: e.target.value })}
                fullWidth
              />
              <TextField
                id="quantity"
                label="موجودی انبار"
                margin="dense"
                color="secondary"
                variant="outlined"
                onChange={(e) => setData({ stock_quantity: e.target.value })}
                fullWidth
              />

              <RadioGroup value={statusValue} onChange={statusChange}>
                <FormControlLabel
                  dir="rtl"
                  value="publish"
                  control={<Radio />}
                  label="منتشر شده"
                />
                <FormControlLabel
                  dir="rtl"
                  value="draft"
                  control={<Radio />}
                  label="پیش نویس"
                />
              </RadioGroup>
              <CKEditor
                config={{
                  contentsLangDirection: 'rtl',
                  language: 'fa',
                }}
                editor={ClassicEditor}
                data="<p>توضیحات محصول جدید</p>"
                onChange={(event, editor) => {
                  const data = editor.getData();
                  setData({ description: data });
                }}
              />
            </StylesProvider>
            <Button
              className={cls.submitButton}
              color="secondary"
              variant="contained"
              onClick={setToDataBase}>
              ثبت محصول
            </Button>
          </Grid>
        </Paper>
      </Container>
    </Grid>
  );
};
