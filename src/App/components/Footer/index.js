import React from 'react';
import { Typography, Grid, Paper } from '@material-ui/core';
import ReactLogo from '../../assets/image/ReactLogo.png';
import MaterialLogo from '../../assets/image/MaterialLogo.png';
import logoilaraweb from '../../assets/logoilaraweb.png';
import styles from './styles';

export default function StickyFooter() {
  //styles
  const cls = styles();
  return (
    <Paper className={cls.root}>
      <Grid
        container
        spacing={2}
        direction="row"
        justify="center"
        alignItems="center">
        <Typography variant="body2" className={cls.text}>
          <a
            href="http://www.ilaraweb.ir"
            rel="noopener noreferrer"
            target="_blank">
            <img
              src={logoilaraweb}
              className={cls.ilarawebLogo}
              alt="ilaraweb"
            />
          </a>
        </Typography>
        <Typography variant="body2" color="initial">
          Powered By
        </Typography>
        <img src={ReactLogo} className={cls.Logo} alt="ilaraweb" />
        <img src={MaterialLogo} className={cls.Logo} alt="ilaraweb" />
      </Grid>
    </Paper>
  );
}
