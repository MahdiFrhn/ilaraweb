import AddProduct from './AddProduct';
import Products from './Products';
import Category from './Category';
import Orders from './Orders';
import Home from './Home';

export default {
  Home,
  Products,
  AddProduct,
  Category,
  Orders
};
