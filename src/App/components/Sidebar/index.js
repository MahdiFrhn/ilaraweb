import React from 'react';
import {
  Divider,
  List,
  ListItemIcon,
  ListItemText,
  Grid,
  ListItem,
  Typography,
} from '@material-ui/core';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import HomeIcon from '@material-ui/icons/Home';
import CategoryIcon from '@material-ui/icons/Category';
import SettingsIcon from '@material-ui/icons/Settings';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import { Link } from 'react-router-dom';
import styles from './styles';

export default function Header() {
  const classes = styles();

  return (
    <div className={classes.root}>
      <div className={classes.toolbar}>
        <Typography variant="h6" color="secondary" className={classes.title}>
          داشبورد مدیریت
        </Typography>
        <Typography variant="body2" color="textSecondary" className={classes.title}>
          نسخه : ۱.۰.۰
        </Typography>
      </div>
      <Divider className={classes.dvider} />
      <Grid container direction="row" justify="center" alignItems="center">
        <List>
          <ListItem button className={classes.item} component={Link} to="./">
            <ListItemIcon>
              <HomeIcon color="secondary" />
            </ListItemIcon>
            <ListItemText primary="صفحه اصلی" />
          </ListItem>

          <ListItem
            button
            className={classes.item}
            component={Link}
            to="./Products">
            <ListItemIcon>
              <ShoppingCartIcon color="secondary" />
            </ListItemIcon>
            <ListItemText primary="محصولات" />
          </ListItem>
          <ListItem
            button
            className={classes.item}
            component={Link}
            to="./Orders">
            <ListItemIcon>
              <FormatListBulletedIcon color="secondary" />
            </ListItemIcon>
            <ListItemText primary="سفارشات" />
          </ListItem>
          <ListItem
            button
            className={classes.item}
            component={Link}
            to="./AddProduct">
            <ListItemIcon>
              <AddShoppingCartIcon color="secondary" />
            </ListItemIcon>
            <ListItemText primary="محصول جدید" />
          </ListItem>
          <ListItem
            button
            className={classes.item}
            component={Link}
            to="./Category">
            <ListItemIcon>
              <CategoryIcon color="secondary" />
            </ListItemIcon>
            <ListItemText primary="دسته بندی ها" />
          </ListItem>
          <ListItem
            button
            className={classes.item}
            component={Link}
            to="./Setting">
            <ListItemIcon>
              <SettingsIcon color="secondary" />
            </ListItemIcon>
            <ListItemText primary="تنظیمات" />
          </ListItem>
        </List>
        <Divider />
      </Grid>
    </div>
  );
}
