import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  root: {
    boxShadow: '0px 0px 5px 0px ',
    borderRadius: 15,
    padding: 15,
  },
  cardcontent: {
    direction: 'rtl',
  },
  media: {
    height: 210,
  },

  divider: {
    marginTop: 15,
    marginBottom: 15,
  },
  price: {
    backgroundColor: '#FFD166',
    borderRadius: 20,
    height: 35,
    marginTop: 15,
  },
  button: {
    borderRadius: 20,
    margin:15

  },
}));
