import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styles from './styles';
import moment from 'jalali-moment';

export default (props) => {
  const cls = styles();
  const [orderData, setOrderData] = useState({
    
  })

  const orders = props.orders;
  //for price - name - quantity
  const detail = orders.map((i) => i.line_items);
  console.log(detail);
  

  //for convert  miladi to Shamsi Data
  const dateArray = orders.map((i) =>
    moment(i.date_created, 'YYYY/MM/DD').locale('fa').format('YYYY/MM/DD')
  );

  return (
    <TableContainer component={Paper} className={cls.container}>
      <Table className={cls.tabel}>
        <TableHead>
          <TableRow>
            <TableCell className={cls.tableCell} align="center">
              تاریخ
            </TableCell>
            <TableCell className={cls.tableCell} align="center">
              قیمت کل
            </TableCell>
            <TableCell className={cls.tableCell} align="center">
              تعداد
            </TableCell>

            <TableCell className={cls.tableCell} align="center">
              نام محصول
            </TableCell>
            <TableCell className={cls.tableCell} align="center">
              تلفن
            </TableCell>
            <TableCell className={cls.tableCell} align="center">
              شهر
            </TableCell>
            <TableCell className={cls.tableCell} align="center">
              نام خانوادگی
            </TableCell>
            <TableCell className={cls.tableCell} align="center">
              نام
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orders.map((row) => (
            <TableRow key={row.billing.first_name}>
              <TableCell className={cls.tableCell} align="center">
                {dateArray}
              </TableCell>
              <TableCell className={cls.tableCell} align="center">
                {row.total}
              </TableCell>
              <TableCell className={cls.tableCell} align="center">
                {row.line_items.quantity}
              </TableCell>

              <TableCell className={cls.tableCell} align="center">
                {row.line_items.name}
              </TableCell>
              <TableCell className={cls.tableCell} align="center">
                {row.billing.phone}
              </TableCell>
              <TableCell className={cls.tableCell} align="center">
                {row.billing.city}
              </TableCell>
              <TableCell className={cls.tableCell} align="center">
                {row.billing.last_name}
              </TableCell>
              <TableCell component="th" scope="row">
                {row.billing.first_name}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

// import React from 'react';
// import { withStyles } from '@material-ui/core/styles';
// import Table from '@material-ui/core/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableHead from '@material-ui/core/TableHead';
// import TableRow from '@material-ui/core/TableRow';
// import Paper from '@material-ui/core/Paper';

// const styles = theme => ({
//   root: {
//     display: 'flex',
//     marginTop: theme.spacing.unit * 3,
//     overflowX: 'hide',
//   },
//   table: {
//     minWidth: 340,
//   },
//   tableCell: {
//     paddingRight: 4,
//     paddingLeft: 5
//   }
// });

// export default (props) => {
//   const orders = props.orders;
//   //for price name quantity
//   const detail = orders.map((i) => i.line_items);
//   console.log(detail);

//   //for convert Data miladi to Shamsi
//   const dateArray = orders.map((i) =>
//     moment(i.date_created, 'YYYY/MM/DD').locale('fa').format('YYYY/MM/DD')
//   );
//   orders.map((i) => {});

//   return (
//     <Paper className={classes.root}>
//       <Table className={classes.table}>
//         <TableHead>
//           <TableRow>
//             <TableCell className={classes.tableCell}>Dessert (100g serving)</TableCell>
//             <TableCell numeric className={classes.tableCell}>Calories</TableCell>
//             <TableCell numeric className={classes.tableCell}>Fat (g)</TableCell>
//             <TableCell numeric className={classes.tableCell}>Carbs (g)</TableCell>
//             <TableCell numeric className={classes.tableCell}>Protein (g)</TableCell>
//           </TableRow>
//         </TableHead>
//         <TableBody>
//           {data.map(n => {
//             return (
//               <TableRow key={n.id}>
//                 <TableCell component="th" scope="row" className={classes.TableCell}>
//                   {n.name}
//                 </TableCell>
//                 <TableCell numeric className={classes.tableCell}>{n.calories}</TableCell>
//                 <TableCell numeric className={classes.tableCell}>{n.fat}</TableCell>
//                 <TableCell numeric className={classes.tableCell}>{n.carbs}</TableCell>
//                 <TableCell numeric className={classes.tableCell}>{n.protein}</TableCell>
//               </TableRow>
//             );
//           })}
//         </TableBody>
//       </Table>
//     </Paper>
//   );
// }

// export default withStyles(styles)(SimpleTable);
