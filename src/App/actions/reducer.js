import Types from './Types';

const initialState = {
  loading: true,
  products: [],
  orders: [],
  categories: [],
};

export const Reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case Types.ADD_PRODUCT:
      return { ...state, products: [...state.products, payload] };

    case Types.SET_PRODUCT:
      return { ...state, products: payload };

    case Types.SET_ORDERS:
      return { ...state, orders: payload };

    case Types.SET_CATEGORIES:
      return { ...state, categories: payload };

    case Types.SET_LOADING:
      return { ...state, loading: payload };

    default:
      return state;
  }
};
