import React from 'react';
import {
  Typography,
  Grid,
  CardMedia,
  CardContent,
  CardActionArea,
  Card,
} from '@material-ui/core';
import styles from './styles';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import { Edit } from '../../components';

export default function (props) {
  //styles
  const cls = styles();
  return (
    <React.Fragment>
      <Card className={cls.root}>
        <CardActionArea>
          <CardMedia className={cls.media} image={props.image} />
          <CardContent className={cls.cardcontent}>
            <Typography dir="rtl" variant="body2" className={cls.title}>
              {props.status === 'publish' ? (
                <FiberManualRecordIcon color="primary" fontSize="inherit" />
              ) : (
                <FiberManualRecordIcon color="secondary" fontSize="inherit" />
              )}
              {`${props.title}`}
            </Typography>
            <Typography dir="rtl" variant="inherit">
              {`موجودی انبار : ${props.quantity} عدد `}
            </Typography>

            <Grid
              className={cls.price}
              container
              justify="center"
              alignItems="center">
              <Typography
                dir="rtl"
                variant="body2"
                color="secondary"
                dangerouslySetInnerHTML={{
                  __html: props.price_html,
                }}
              />
            </Grid>
          </CardContent>
        </CardActionArea>
        <Edit
          id={props.id}
          image={props.image}
          title={props.title}
          price={props.price}
          status={props.status}
        />
      </Card>
    </React.Fragment>
  );
}
