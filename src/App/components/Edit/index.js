import React, { useState } from 'react';
import {
  Typography,
  Grid,
  Button,
  CardActions,
  DialogTitle,
  DialogContent,
  DialogActions,
  Dialog,
  TextField,
  Divider,
  FormControlLabel,
  RadioGroup,
  Radio,
} from '@material-ui/core';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Alert from '@material-ui/lab/Alert';
import EditIcon from '@material-ui/icons/Edit';
import styles from './styles';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import { create } from 'jss';
import rtl from 'jss-rtl';
import { StylesProvider, jssPreset } from '@material-ui/core/styles';
import { editProducts, deleteProducts } from '../../actions';
// Configure JSS for RTL Text field label
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
export default (props) => {
  const cls = styles();
  const [open, setOpen] = useState(false);
  const [openRemove, setOpenRemove] = useState(false);
  const [priceData, setPriceData] = useState();
  const [titleData, setTitleChange] = useState();
  const [quantity, setQuantity] = useState();
  const [statusValue, setStatusValue] = useState(props.status);
  // price change
  const priceChange = (e) => {
    setPriceData(e.target.value);
  };
  //title change
  const titleChange = (e) => {
    setTitleChange(e.target.value);
  };
  //quantity change
  const quantityChange = (e) => {
    setQuantity(e.target.value);
  };
  //value for Radio Button
  const statusChange = (e) => {
    setStatusValue(e.target.value);
  };

  const data = {
    name: titleData,
    regular_price: priceData,
    status: statusValue,
    stock_quantity: quantity,
  };

  //edit DIALOG handle

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleRemoveOpen = () => {
    setOpenRemove(true);
  };
  const handleRemoveClose = () => {
    setOpenRemove(false);
  };

  // PUT ON DATA --for Edit products
  const id = props.id;
  const onEdit = () => {
    editProducts(id, data);
  };

  // DELETE ON DATA --for delete products
  const onDelete = () => {
    deleteProducts(id);
  };

  return (
    <div>
      <Grid
        container
        spacing={1}
        direction="row"
        justify="center"
        alignItems="center">
        <CardActions>
          <Button
            variant="contained"
            color="secondary"
            size="small"
            onClick={handleRemoveOpen}
            className={cls.button}
            startIcon={<DeleteForeverIcon />}>
            حذف
          </Button>
          <Button
            variant="contained"
            color="secondary"
            size="small"
            className={cls.button}
            startIcon={<EditIcon />}
            onClick={handleClickOpen}>
            ویرایش
          </Button>
        </CardActions>
      </Grid>

      <Dialog open={open} onClose={handleClose}>
        <Grid container direction="column" justify="center" alignItems="center">
          <DialogTitle>ویرایش اطلاعات محصول</DialogTitle>
          <img className={cls.media} src={props.image} alt="WooApp" />
          <Typography dir="rtl" variant="body1">
            {`${props.title}`}
          </Typography>
          <DialogContent>
            <Divider className={cls.divider} />
            <StylesProvider jss={jss}>
              <TextField
                color="secondary"
                dir="rtl"
                variant="outlined"
                margin="dense"
                id="name"
                label="نام محصول"
                defaultValue={props.title}
                onChange={titleChange}
                fullWidth
              />
              <TextField
                color="secondary"
                variant="outlined"
                margin="dense"
                id="price"
                label="قیمت محصول"
                placeholder={props.price}
                fullWidth
                onChange={priceChange}
              />
              <TextField
                color="secondary"
                variant="outlined"
                margin="dense"
                id="quantity"
                label="تعداد موجودی انبار "
                placeholder={props.quantity}
                fullWidth
                onChange={quantityChange}
              />

              <Divider className={cls.divider} />
              {props.status === 'draft' ? (
                <Typography dir="rtl" variant={'body2'} color="textSecondary">
                  وضعیت قبلی محصول :
                  <FiberManualRecordIcon color="secondary" fontSize="inherit" />
                  منتشر نشده
                </Typography>
              ) : (
                <Typography dir="rtl" variant={'body2'} color="textSecondary">
                  وضعیت قبلی محصول :
                  <FiberManualRecordIcon color="primary" fontSize="inherit" />
                  منتشر شده
                </Typography>
              )}
              <RadioGroup value={statusValue} onChange={statusChange}>
                <FormControlLabel
                  dir="rtl"
                  value="publish"
                  control={<Radio />}
                  label="منتشر شده"
                />
                <FormControlLabel
                  dir="rtl"
                  value="draft"
                  control={<Radio />}
                  label="پیش نویس"
                />
              </RadioGroup>
            </StylesProvider>
          </DialogContent>
        </Grid>
        <DialogActions>
          <Divider className={cls.divider} />
          <Button
            onClick={handleClose}
            variant="contained"
            size="large"
            className={cls.button}
            color="secondary">
            لغو
          </Button>
          <Button
            onClick={onEdit}
            variant="contained"
            size="large"
            className={cls.button}
            color="secondary">
            ثبت
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog open={openRemove} onClose={handleRemoveClose}>
        <DialogTitle dir="rtl">{'حذف محصول'}</DialogTitle>
        <DialogContent dir="rtl">
          <Alert variant="filled" dir="rtl" severity="warning">
            آیا از حذف این محصول اطمینان دارید؟
          </Alert>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            onClick={handleRemoveClose}
            color="secondary">
            لغو
          </Button>
          <Button
            variant="contained"
            onClick={onDelete}
            color="secondary"
            autoFocus>
            تایید
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
