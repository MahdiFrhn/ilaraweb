import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  root: {
    position: 'fixed',
    bottom: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: "100%",
    height: 30
  },
  love: {
    color: '#ff0000',
  },
  Logo: {
    width: 20,
    marginLeft: 5,
  },
  ilarawebLogo: {
    width: 80,
  },
  text: {
    color: '#525473',
  },
}));
