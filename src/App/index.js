import React, { useState } from 'react';
import routes from './routes';
import { Menu, SideBar } from './components';
import { HashRouter as Router, Route } from 'react-router-dom';
import { MuiThemeProvider, Grid, IconButton } from '@material-ui/core';
import Brightness4Icon from '@material-ui/icons/Brightness4';
import Brightness7Icon from '@material-ui/icons/Brightness7';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Footer } from '../App/components';
import themeStyle from '../theme';

export default function () {
  const [theme, setTheme] = useState(true);
  const icon = !theme ? (
    <Brightness7Icon color="secondary" />
  ) : (
    <Brightness4Icon color="primary" />
  );

  return (
    <MuiThemeProvider theme={theme ? themeStyle.default : themeStyle.dark}>
      <CssBaseline />
      <Router>
        <Menu side={<SideBar />} />
        <IconButton
          style={{ marginTop: 60, position: 'fixed' }}
          edge="start"
          onClick={() => setTheme(!theme)}>
          {icon}
        </IconButton>
        {routes.map((i) => (
          <Route key={i.path} path={i.path} component={i.component} exact />
        ))}
      </Router>
      <Grid container justify="center" alignItems="center">
        <Footer />
      </Grid>
    </MuiThemeProvider>
  );
}
